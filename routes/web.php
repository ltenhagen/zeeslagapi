<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Zeeslag API V0.1 - Made by Lorenzo ten Hagen';
});

$router->post('/addplayer', 'GameController@addPlayer');
$router->post('/player/check', 'GameController@playerCheck');

$router->post('/test', 'GameController@checkForPlayers');
