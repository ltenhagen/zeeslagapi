<?php


namespace App\Http\Controllers;

use App\Player;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class GameController
 *
 * @package App\Http\Controllers
 */
class GameController
{
    public function playerCheck()
    {
        if ($this->checkForPlayers()) {
            return 'set_ships';
        }

        return 'wait';
    }

    public function addPlayer()
    {
        $uuid = request()->input('uuid') ?? '';

        if ($uuid === "") {
            return 'error';
        }

        try {
            Player::create([
                'uuid' => $uuid,
                'is_online' => true
            ]);
        } catch (Exception $exception) {
            return 'error';
        }

        if ($this->checkForPlayers()) {
            return 'set_ships';
        }

        return 'wait';
    }

    public function checkForPlayers()
    {
        $players = DB::table('players')->get();

        if ((count($players->all()) % 2) === 0) {
            return true;
        }

        return false;
    }
}
